using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OpenAI_API;
using OpenAI_API.Chat;
using TMPro;
using UnityEngine.UI;
using System;
using OpenAI_API.Models;

public class OpenAIController : MonoBehaviour
{
    public int npcNumb;

    public string[] personality;
    public string[] context;
    public string[] npcName;

    public TMP_Text textField;
    public TMP_InputField inputField;
    public Button okButton;

    private OpenAIAPI api;
    private List<ChatMessage> messages;

    public float temperature;
    public int maxTokens;

    public GameObject doorObject;

    private void OnEnable()
    {
        npcNumb = InteractionScript.npcNumb;
        api = new OpenAIAPI(Environment.GetEnvironmentVariable("OPENAI_API_KEY", EnvironmentVariableTarget.User));
        StartConversation();
        okButton.onClick.AddListener(() => GetResponse());

    }


    private void StartConversation()
    {
        messages = new List<ChatMessage>
        {
            new ChatMessage(ChatMessageRole.System, personality[npcNumb])
        };

        inputField.text = "";
        string startString = context[npcNumb];
        textField.text = startString;


    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            GetResponse();
        }
    }


    private async void GetResponse()
    {
        if (inputField.text.Length < 1)
        {
            return;
        }

        okButton.enabled = false;

        ChatMessage userMessage = new ChatMessage();
        userMessage.Role = ChatMessageRole.User;
        userMessage.Content = inputField.text;
        if (userMessage.Content.Length > 100)
        {
            userMessage.Content = userMessage.Content.Substring(0, 100);
        }
        Debug.Log(string.Format("{0}: {1}", userMessage.rawRole, userMessage.Content));

        messages.Add(userMessage);

        textField.text = string.Format("You: {0}", userMessage.Content);

        inputField.text = "";
        var chatResult = await api.Chat.CreateChatCompletionAsync(new ChatRequest()
        {
            Model = Model.ChatGPTTurbo,
            Temperature = temperature,
            MaxTokens = maxTokens,
            Messages = messages
        });

        ChatMessage responseMessage = new ChatMessage();
        responseMessage.Role = chatResult.Choices[0].Message.Role;
        responseMessage.Content = chatResult.Choices[0].Message.Content;
        Debug.Log(string.Format("{0}: {1}", responseMessage.rawRole, responseMessage.Content));

        messages.Add(responseMessage);

        textField.text = string.Format("You: {0}\n\n" + npcName[npcNumb] + ": {1}", userMessage.Content, responseMessage.Content);

        string responseMessageString = responseMessage.Content;
        string userMessageString = userMessage.Content;

        if (responseMessageString.Contains("jeronimo") && npcNumb == 0 || userMessageString.Contains("jeronimo") && npcNumb == 0 || responseMessageString.Contains("Jeronimo") && npcNumb == 0 || userMessageString.Contains("Jeronimo") && npcNumb == 0)
        {
            DoorOpen();
        }

        okButton.enabled = true;
    }

    private void DoorOpen()
    {
            doorObject.SetActive(false);
            Debug.Log("open");
    }

}
