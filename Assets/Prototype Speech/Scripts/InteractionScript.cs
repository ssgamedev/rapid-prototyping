using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionScript : MonoBehaviour
{
    public Camera mainCamera;
    public LayerMask npcMask;
    public LayerMask fruitMask;
    public float maxDistance = 10f;

    bool interactable;

    public GameObject interactText;
    public GameObject pearText;
    public GameObject dialoguePanel;
    public GameObject pearObject;

    public Button quitButton;

    public string targetName;

    public TMPro.TextMeshProUGUI nameTag;

    public static int npcNumb;

    public bool pear = false;
    public bool pearHover = false;

    private void Start()
    {
        quitButton.onClick.AddListener(quitButtonClicked);
    }

    private void Update()
    {
        Ray ray = new Ray(mainCamera.transform.position, mainCamera.transform.forward);
        if(Physics.Raycast(ray, out RaycastHit hit, maxDistance, npcMask))
        {
            if (hit.distance <= maxDistance)
            {
                interactable = true;
                targetName = hit.collider.gameObject.name;
            }
        }

        else
        {
            interactable = false;
        }


        if (Physics.Raycast(ray, out RaycastHit hit2, maxDistance, fruitMask))
        {
            if (hit.distance <= maxDistance)
            {
                pearHover = true;
                targetName = hit2.collider.gameObject.name;
            }
        }

        else
        {
            pearHover = false;
        }

        if (pearHover)
        {
            pearText.SetActive(true);
        }
        else
        {
            pearText.SetActive(false);
        }

        if (interactable)
        {
            interactText.SetActive(true);
        }
        else
        {
            interactText.SetActive(false);
        }

        if (Input.GetKeyDown("escape"))
        {
            quitButtonClicked();
        }

        if (Input.GetKeyDown("e") && interactable)
        {

            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            if(targetName == "Ambatu")
            {
                npcNumb = 0;
                Debug.Log("npc: " + npcNumb);
            }
            else if(targetName == "Quandale")
            {
                npcNumb = 1;
                Debug.Log("npc: " + npcNumb);
            }
            else if (targetName == "Amber" && pear)
            {
                npcNumb = 3;
                Debug.Log("npc: " + npcNumb);
            }
            else if (targetName == "Amber")
            {
                npcNumb = 2;
                Debug.Log("npc: " + npcNumb);
            }
            dialoguePanel.SetActive(true);
            nameTag.text = targetName;
        }

        if (Input.GetKeyDown("e") && pearHover)
        {
            Destroy(pearObject);
            pear = true;
        }

    }

    void quitButtonClicked()
    {
        dialoguePanel.SetActive(false);
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
    }

}
