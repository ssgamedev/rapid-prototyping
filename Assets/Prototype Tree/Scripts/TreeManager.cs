using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeManager : MonoBehaviour
{

    public GameObject treeObject;
    public GameObject leavesObject;
    public GameObject treeDestination;
    public Vector3 treeDestinationVector3;

    void Start()
    {
        leavesObject.SetActive(false);
        treeDestinationVector3 = treeDestination.transform.position;
        StartCoroutine(LerpPosition(treeDestinationVector3, 10));
    }

    IEnumerator LerpPosition(Vector3 targetPosition, float duration)
    {
        float time = 0;
        Vector3 startPosition = transform.position;
        while (time < duration)
        {
            transform.position = Vector3.Lerp(startPosition, targetPosition, time / duration);
            time += Time.deltaTime;
            yield return StartCoroutine(Grow());
        }
        transform.position = targetPosition;
    }

    IEnumerator Grow()
    {
        leavesObject.SetActive(true);
        yield return null;
    }
}
