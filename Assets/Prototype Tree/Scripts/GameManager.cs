using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{


    public int[] treeStage;
    public bool[] harvester;
    public bool[] fruitActive;

    public GameObject[] fruitList;
    public GameObject[] potList;
    public GameObject focusObject;
    public GameObject treeObject;
    public GameObject harvesterObject;

    public Vector3 focusObjectPosition;
    public int focusPotIndex = -1;
    public GameObject focus;

    public int tool;



    //1 = hoe, 2 = seed, 3 = water, 4 = basket

    public int money;

    //UI Shit

    public TMPro.TextMeshProUGUI moneyText;
    public Button hoeButton;
    public Button seedButton;
    public Button waterButton;
    public Button harvestButton;
    public Button harvesterButton;



    void Start()
    {        
        potList = GameObject.FindGameObjectsWithTag("Pot");
        List<GameObject> potsList = new List<GameObject>();

        foreach (GameObject obj in potList)
        {
            potsList.Add(obj);
        }

        fruitList = GameObject.FindGameObjectsWithTag("Fruit");
        List<GameObject> fruitsList = new List<GameObject>();

        foreach (GameObject obj in fruitList)
        {
            fruitsList.Add(obj);
        }

        hoeButton.onClick.AddListener(HoeButtonClicked);
        seedButton.onClick.AddListener(SeedButtonClicked);
        waterButton.onClick.AddListener(WaterButtonClicked);
        harvestButton.onClick.AddListener(HarvestButtonClicked);
        harvesterButton.onClick.AddListener(HarvesterButtonClicked);

        for (int i = 0; i < fruitList.Length; i++)
        {
            fruitList[i].SetActive(false);
        }


    }

    void HoeButtonClicked()
    {
        Hoe(focusPotIndex);
    }

    void SeedButtonClicked()
    {
        Seed(focusPotIndex);
    }

    void WaterButtonClicked()
    {
        Water(focusPotIndex);
    }

    void HarvestButtonClicked()
    {
        Harvest(focusPotIndex);
    }

    void HarvesterButtonClicked()
    {
        Harvester(focusPotIndex);
    }

    void Update()
    {

        //for (int i = 0; i < fruitList.Length; i++)
        //{
        //    if(fruitList[i].activeSelf == true)
        //    {
        //        fruitActive[i] = true;
        //    }

        //    //if (fruitList[i].activeSelf == false)
        //    //{
        //    //    fruitActive[i] = false;
        //    //}

        //}

        if (Input.GetMouseButtonDown(0))
            Focuser();

        moneyText.text = "Money: " + money;

    }

    void Focuser()
    {
        for (int i = 0; i < potList.Length; i++)
        {
            if (potList[i] == focusObject)
            {
                focusPotIndex = i;
                Debug.Log(focusPotIndex);
                break;
            }
        }

        focusObject = ObjectDetection.focusObject;
        focusObjectPosition = focusObject.transform.position;
        focus.transform.position = focusObjectPosition;

    }

    void Hoe(int potIndex)
    {
        if (treeStage[potIndex] == 0)
        {
            treeStage[potIndex] = 1;
        }
    }

    void Seed(int potIndex)
    {
        if(money >= 100 && treeStage[potIndex] == 1)
        {
            money -= 100;
            treeStage[potIndex] = 2;

        }
    }

    void Water(int potIndex)
    {
        if (treeStage[potIndex] == 2)
        {
            treeStage[potIndex] = 3;
            StartCoroutine(Growing(potIndex));
        }
    }

    void Harvest(int potIndex)
    {
        if (fruitActive[potIndex] == true)
        {
            money += 10;
            fruitActive[potIndex] = false;
            fruitList[potIndex].SetActive(false);
        }
    }

    void Harvester(int potIndex)
    {
        if (money >= 50 && harvester[potIndex] == false && treeStage[potIndex] == 4)
        {
            harvester[potIndex] = true;
            money -= 50;
            StartCoroutine(Harvesting(potIndex));
            Vector3 positionToSpawn = potList[potIndex].transform.position;
            Instantiate(harvesterObject, positionToSpawn, Quaternion.identity);
        }
    }

    IEnumerator Growing(int potIndex)
    {
        Vector3 positionToSpawn = potList[potIndex].transform.position;
        Instantiate(treeObject, positionToSpawn, Quaternion.identity);

        yield return new WaitForSeconds(10);

        treeStage[potIndex] = 4;
        StartCoroutine(Grown(potIndex));
    }

    IEnumerator Grown(int potIndex)
    {


        int waitTime = Random.Range(5, 16);

        yield return new WaitForSeconds(waitTime);

        fruitList[potIndex].SetActive(true);
        fruitActive[potIndex] = true;

        StartCoroutine(Grown(potIndex));

    }

    IEnumerator Harvesting(int potIndex)
    {
        yield return new WaitForSeconds(2.5f);
        Harvest(potIndex);
        StartCoroutine(Harvesting(potIndex));

    }

}
