using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Prototype4
{
    public class SpawnManager : MonoBehaviour
    {
        public GameObject enemyPrefab;
        public GameObject enemyPrefab2;
        public GameObject powerupPrefab;
        private float spawnRange = 9;
        public int enemyCount;
        public int waveNumber = 1;

        void Start()
        {
            SpawnEnemyWave(waveNumber);
            Instantiate(powerupPrefab, GenerateSpawnPosition(), powerupPrefab.transform.rotation);

        }

        private void Update()
        {
            enemyCount = FindObjectsOfType<Enemy>().Length;
            if (enemyCount == 0 && PlayerController.player1Alive == true)
            {
                waveNumber++;
                SpawnEnemyWave(waveNumber);
                Instantiate(powerupPrefab, GenerateSpawnPosition(), powerupPrefab.transform.rotation);
            }
            if(MainMenu.multiplayerMode == true)
            {
                if (enemyCount == 0 && Player2Controller.player2Alive == true)
                {
                    waveNumber++;
                    SpawnEnemyWave(waveNumber);
                    Instantiate(powerupPrefab, GenerateSpawnPosition(), powerupPrefab.transform.rotation);
                }
            }

        }

        void SpawnEnemyWave(int enemiesToSpawn)
        {
            for (int i = 0; i < enemiesToSpawn; i++)
            {
                Instantiate(enemyPrefab, GenerateSpawnPosition(), enemyPrefab.transform.rotation);
                if(MainMenu.multiplayerMode == true)
                {
                    Instantiate(enemyPrefab2, GenerateSpawnPosition(), enemyPrefab.transform.rotation);
                }
            }
        }

        private Vector3 GenerateSpawnPosition()
        {
            float spawnPosX = Random.Range(-spawnRange, spawnRange);
            float spawnPosZ = Random.Range(-spawnRange, spawnRange);
            Vector3 randomPos = new Vector3(spawnPosX, 0 , spawnPosZ);
            return randomPos;
        }
    }
}
