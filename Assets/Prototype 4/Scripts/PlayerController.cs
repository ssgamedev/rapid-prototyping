using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Prototype4
{
    public class PlayerController : MonoBehaviour
    {
        private Rigidbody playerRb;
        public float speed = 5.0f;
        private GameObject focalPoint;
        public bool hasPowerup;
        private float powerupStrength = 15.0f;

        public GameObject powerupIndicator;
        public GameObject playerDeathScreen;
        public GameObject playerLoseScreen;

        public GameObject singleplayerScorePanel;
        public GameObject multiplayerScorePanel;

        public TMPro.TextMeshProUGUI scoreDisplay;
        public TMPro.TextMeshProUGUI coopScoreDisplay;

        public static bool player1Alive = true;

        private void Start()
        {
            playerRb = GetComponent<Rigidbody>();
            focalPoint = GameObject.Find("FocalPoint");
        }

        private void Update()
        {
            float forwardInput = Input.GetAxis("Vertical");
            playerRb.AddForce(focalPoint.transform.forward * speed * forwardInput);
            powerupIndicator.transform.position = transform.position + new Vector3(0, -0.5f, 0);

            //Game Over Ui Handler

            if (transform.position.y < -10)
            {   
                player1Alive = false;

                if (MainMenu.vsMode == true)
                {
                    PlayerScreen(1);
                }
                if (MainMenu.vsMode == false && MainMenu.multiplayerMode == true)
                {
                    PlayerScreen(2);
                }
            }

            //Multiplayer Score

            if (player1Alive == false && Player2Controller.player2Alive == false && MainMenu.vsMode == false)
            {
                multiplayerScorePanel.SetActive(true);
                coopScoreDisplay.text = "score: " + Enemy.score;

                GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
                foreach (GameObject enemy in enemies)
                    GameObject.Destroy(enemy);
            }

            //Singleplayer Score

            if (player1Alive == false && MainMenu.multiplayerMode == false)
            {
                singleplayerScorePanel.SetActive(true);
                scoreDisplay.text = "score: " + Enemy.score;

                GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
                foreach (GameObject enemy in enemies)
                    GameObject.Destroy(enemy);
            }

        }

        private void PlayerScreen(int status)
        {
            if(status == 1 && Player2Controller.player2Alive == true)
            {
                playerLoseScreen.SetActive(true);
            }
            if(status == 2)
            {
                playerDeathScreen.SetActive(true);

            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Powerup"))
            {
                hasPowerup = true;
                Destroy(other.gameObject);
                StartCoroutine(PowerUpCountdownRoutine());
                powerupIndicator.gameObject.SetActive(true);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if(collision.gameObject.CompareTag("Enemy") && hasPowerup)
            {
                Rigidbody enemyRigidbody = collision.gameObject.GetComponent<Rigidbody>();
                Vector3 awayFromPlayer = (collision.gameObject.transform.position - transform.position);

                Debug.Log("Collided with " + collision.gameObject.name + " with powerup set to " + hasPowerup);
                enemyRigidbody.AddForce(awayFromPlayer * powerupStrength, ForceMode.Impulse);
            }
        }

        IEnumerator PowerUpCountdownRoutine()
        {
            yield return new WaitForSeconds(7);
            hasPowerup = false;
            powerupIndicator.gameObject.SetActive(false);
        }
    }
}

