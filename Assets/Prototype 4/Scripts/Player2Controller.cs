using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Prototype4
{
    public class Player2Controller : MonoBehaviour
    {
        private Rigidbody playerRb;
        public float speed = 5.0f;
        private GameObject focalPoint;
        public bool hasPowerup;
        private float powerupStrength = 15.0f;
        public GameObject powerupIndicator;
        public GameObject playerDeathScreen;
        public GameObject playerLoseScreen;

        public static bool player2Alive = true;

        private void Start()
        {
            playerRb = GetComponent<Rigidbody>();
            focalPoint = GameObject.Find("FocalPoint 2");
        }

        private void Update()
        {
            float forwardInput = Input.GetAxis("Vertical2");
            playerRb.AddForce(focalPoint.transform.forward * speed * forwardInput);
            powerupIndicator.transform.position = transform.position + new Vector3(0, -0.5f, 0);

            if (transform.position.y < -10)
            {
                player2Alive = false;
                
                if (MainMenu.vsMode == true)
                {
                    PlayerScreen(1);
                }
                if (MainMenu.vsMode == false)
                {
                    PlayerScreen(2);
                }
            }
        }

        private void PlayerScreen(int status)
        {
            if (status == 1 && PlayerController.player1Alive == true)
            {
                playerLoseScreen.SetActive(true);
            }
            if (status == 2)
            {
                playerDeathScreen.SetActive(true);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Powerup"))
            {
                hasPowerup = true;
                Destroy(other.gameObject);
                StartCoroutine(PowerUpCountdownRoutine());
                powerupIndicator.gameObject.SetActive(true);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Enemy") && hasPowerup)
            {
                Rigidbody enemyRigidbody = collision.gameObject.GetComponent<Rigidbody>();
                Vector3 awayFromPlayer = (collision.gameObject.transform.position - transform.position);

                Debug.Log("Collided with " + collision.gameObject.name + " with powerup set to " + hasPowerup);
                enemyRigidbody.AddForce(awayFromPlayer * powerupStrength, ForceMode.Impulse);
            }
        }

        IEnumerator PowerUpCountdownRoutine()
        {
            yield return new WaitForSeconds(7);
            hasPowerup = false;
            powerupIndicator.gameObject.SetActive(false);
        }
    }
}
