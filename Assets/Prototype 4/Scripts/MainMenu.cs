using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour
{

    public Button singlePlayerButton;
    public Button coopButton;
    public Button vsButton;
    public static bool vsMode = false;
    public static bool multiplayerMode = false;

    void Start()
    {
        singlePlayerButton.onClick.AddListener(SinglePlayer);
        coopButton.onClick.AddListener(CoopMode);
        vsButton.onClick.AddListener(VsMode);

    }


    void SinglePlayer()
    {
        SceneManager.LoadScene("Prototype 4");
    }

    void CoopMode()
    {
        SceneManager.LoadScene("Prototype 4 Multiplayer");
        multiplayerMode = true;
}

    void VsMode()
    {
        SceneManager.LoadScene("Prototype 4 Multiplayer");
        multiplayerMode = true;
        vsMode = true;
    }

}
