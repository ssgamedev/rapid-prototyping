using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Prototype4
{
    public class RotateCamera : MonoBehaviour
    {
        public float rotationSpeed;

        void Update()
        {
            float horizontalInput = Input.GetAxis("Horizontal");
            transform.Rotate(Vector3.up, horizontalInput * rotationSpeed * Time.deltaTime);
        }
    }

}

