using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Prototype4
{
    public class Enemy : MonoBehaviour
    {
        public float speed = 0.5f;
        private Rigidbody enemyRb;
        private GameObject player;
        public static int score = 0;

        private void Start()
        {
            enemyRb = GetComponent<Rigidbody>();
            player = GameObject.Find("Player");
        }

        private void Update()
        {
            if (transform.position.y < -10)
            {
                Destroy(gameObject);
                score++;
            }
            Vector3 lookDirection = (player.transform.position - transform.position).normalized;
            enemyRb.AddForce(lookDirection * speed);
        }
    }
}

