using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindManager : MonoBehaviour
{

    public Rigidbody playerRb;

    public int randDuration;
    public float randDirection;
    public float randStrength;

    public TMPro.TextMeshProUGUI windText;


    void Start()
    {
        randDirection = 0;
        randDuration = 5;
        playerRb = GetComponent<Rigidbody>();
        StartCoroutine(WindGenerator());
    }
    
    IEnumerator WindGenerator()
    {        
        
        yield return new WaitForSeconds(randDuration);
        randDuration = Random.Range(5, 21);
        randDirection = Random.Range(-1, 2);
        randStrength = Random.Range(1, 3);

        //Wind(randDirection, randStrength);

        StartCoroutine(WindGenerator());    
    }

    private void Update()
    {
        switch (randDirection)
        {
            case -1:
                windText.text = string.Format("Wind: <");
                break;
            case 0:
                windText.text = string.Format("Wind: ~");
                break;
            case 1:
                windText.text = string.Format("Wind: >");
                break;
        }
        playerRb.AddTorque(transform.forward * randStrength/10 * randDirection);
    }

    //private void Wind(float direction, float strength)
    //{
    //    switch (direction)
    //    {
    //        case -1:
    //            windText.text = string.Format("Wind: <");
    //            break;
    //        case 0:
    //            windText.text = string.Format("Wind: ~");
    //            break;
    //        case 1:
    //            windText.text = string.Format("Wind: >");
    //            break;
    //    }
    //    playerRb.AddTorque(transform.forward * strength * direction);
    //}

}
