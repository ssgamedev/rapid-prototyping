using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour
{

    public GameObject obstacleLeft;
    public GameObject obstacleRight;
    public GameObject obstacleDown;

    private int randTime;
    private int randObstacle;

    public Vector3 startPosition;

    GameObject obstacleToSpawn;


    void Start()
    {
        startPosition = GameObject.Find("Spawn Point").transform.position;
        StartCoroutine(Spawner());
    }

    IEnumerator Spawner()
    {
        randObstacle = Random.Range(0, 3);

        switch (randObstacle)
        {
            case 0:
                obstacleToSpawn = obstacleLeft;
                break;
            case 1:
                obstacleToSpawn = obstacleRight;
                    break;
            case 2:
                obstacleToSpawn = obstacleDown;
                break;
        }
            


        Instantiate(obstacleToSpawn, startPosition, Quaternion.identity);

        randTime = Random.Range(3, 5);
        yield return new WaitForSeconds(randTime);
        StartCoroutine(Spawner());
    }


}
