using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleScript : MonoBehaviour
{
    public int speed;

    public Transform endPoint;

    private void Update()
    {
        endPoint = GameObject.Find("Despawner").transform;
        transform.position = Vector3.MoveTowards(transform.position, endPoint.position, speed * Time.deltaTime);
    }


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("intrigger");
        if (other.gameObject.CompareTag("Despawner"))
        {
            Destroy(gameObject);
        }
    }

}
