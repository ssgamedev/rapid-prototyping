using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_Script : MonoBehaviour
{
    public float torque;
    public Rigidbody playerRb;
    public Transform centerOfMassPos;

    public float targetSpeed;
    public Transform target;

    public int jumpAmount;

    void Start()
    {
        playerRb = GetComponent<Rigidbody>();
        centerOfMassPos = GameObject.Find("Center Of Mass").transform;
        playerRb.centerOfMass = centerOfMassPos.localPosition;
    }


    void Update()
    {
        float turn = Input.GetAxis("Horizontal");
        playerRb.AddTorque(transform.forward * torque * turn);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            playerRb.AddForce(Vector3.up * jumpAmount, ForceMode.Impulse);
        }

        Vector3 eulerAngles = transform.rotation.eulerAngles;
        eulerAngles.y = 180;
        eulerAngles.x = 0;


        target.transform.rotation = Quaternion.Euler(eulerAngles);

        //Vector3 targetDirection = target.position - transform.position;
        float singleStep = targetSpeed * Time.deltaTime;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, target.rotation, singleStep);
        //transform.rotation = Quaternion.LookRotation(newDirection);

        if (transform.position.y < -2)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }


    }
}
