using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PivotGameManager : MonoBehaviour
{

    public GameObject panel;
    public Button playButton;


    void Start()
    {
        Time.timeScale = 0;
        playButton.onClick.AddListener(PlayButtonClicked);
    }

    void PlayButtonClicked()
    {
        panel.SetActive(false);
        Time.timeScale = 1;
    }
}
