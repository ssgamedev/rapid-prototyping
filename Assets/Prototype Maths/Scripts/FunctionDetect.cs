using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionDetect : MonoBehaviour
{

    public static int function;

    // 1 = Addition, 2 = Subtraction, 3 = Multiplication, 4 = Division

    public static bool functionPresent;


    void Start()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.CompareTag("Plus"))
        {
            function = 1;
            functionPresent = true;
        }

        if (other.gameObject.CompareTag("Minus"))
        {
            function = 2;
            functionPresent = true;
        }

        if (other.gameObject.CompareTag("Times"))
        {
            function = 3;
            functionPresent = true;
        }

        if (other.gameObject.CompareTag("Divide"))
        {
            function = 4;
            functionPresent = true;
        }

    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("Divide") || other.gameObject.CompareTag("Times") || other.gameObject.CompareTag("Minus") || other.gameObject.CompareTag("Plus"))
        {
            functionPresent = false;
        }

     }

}
