using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MathsHandler : MonoBehaviour
{

    private int functionRand;

    // 1 = Addition, 2 = Subtraction, 3 = Multiplication, 4 = Division

    private int numb1;
    private int numb2;
    private int numb3;

    //private int falseNumb1;
    //private int falseNumb2;
    //private int falseNumb3;

    private int problemNumb1;
    private int problemNumb2;
    private int problemNumb3;


    private int function;

    // 1 = Addition, 2 = Subtraction, 3 = Multiplication, 4 = Division

    private int panel1Result;
    private int panel2Result;
    private int panel3Result;

    private bool functionPresent;
    private bool numb1Present;
    private bool numb2Present;
    private bool numb3Present;

    public TMPro.TextMeshPro numb1Text;
    public TMPro.TextMeshPro numb2Text;
    public TMPro.TextMeshPro numb3Text;
    public TMPro.TextMeshPro falseNumb1Text;
    public TMPro.TextMeshPro falseNumb2Text;
    public TMPro.TextMeshPro falseNumb3Text;

    public GameObject correctText;
    public Button button;

    void Start()
    {
        MathsGenerator();

        button.onClick.AddListener(ButtonClicked);

        correctText.SetActive(false);
    }

    private void Update()
    {
        if (functionPresent == true && numb1Present == true && numb2Present == true && numb3Present == true)
        {
            MathsExecutor();
        }

        function = FunctionDetect.function;
        functionPresent = FunctionDetect.functionPresent;

        numb1Present = PanelDetect.numb1Present;
        numb2Present = PanelDetect.numb2Present;
        numb3Present = PanelDetect.numb3Present;

        panel1Result = PanelDetect.panel1Result;
        panel2Result = PanelDetect.panel2Result;
        panel3Result = PanelDetect.panel3Result;

    }

    private void MathsGenerator()
    {

        functionRand = Random.Range(1, 3);

        numb1 = Random.Range(1, 24);

        numb2 = Random.Range(1, 24);

        //falseNumb1 = Random.Range(1, 24);

        //falseNumb2 = Random.Range(1, 24);

        //falseNumb3 = Random.Range(1, 24);

        if (functionRand == 2)
        {
            numb3 = numb1 * numb2;

            Debug.Log("Problem is " + numb1 + " * " + numb2 + " = " + numb3 + " OR " + numb3 + " / " + numb2 + " = " + numb1);
        }

        else
        {
            numb3 = numb1 + numb2;

            Debug.Log("Problem is " + numb1 + " + " + numb2 + " = " + numb3 + " OR " + numb3 + " - " + numb2 + " = " + numb1);
        }

        Debug.Log(numb1+ ", " + numb2 + ", " + numb3);

        numb1Text.text = "" + numb1;
        numb2Text.text = "" + numb2;
        numb3Text.text = "" + numb3;
        //falseNumb1Text.text = "" + falseNumb1;
        //falseNumb2Text.text = "" + falseNumb2;
        //falseNumb3Text.text = "" + falseNumb3;
}

    private void MathsExecutor()
    {
        //Place Numbers in Order
        switch (panel1Result)
        {
            case 1:
                problemNumb1 = numb1;
                break;
            case 2:
                problemNumb1 = numb2;
                break;
            case 3:
                problemNumb1 = numb3;
                break;
        }

        switch (panel2Result)
        {
            case 1:
                problemNumb2 = numb1;
                break;
            case 2:
                problemNumb2 = numb2;
                break;
            case 3:
                problemNumb2 = numb3;
                break;
        }

        switch (panel3Result)
        {
            case 1:
                problemNumb3 = numb1;
                break;
            case 2:
                problemNumb3 = numb2;
                break;
            case 3:
                problemNumb3 = numb3;
                break;
        }

        //Execute maths

        switch (function)
        {
            case 1:

                if (problemNumb1 + problemNumb2 == problemNumb3)
                {
                    correctText.SetActive(true);
                }

                break;
            case 2:

                if (problemNumb1 - problemNumb2 == problemNumb3)
                {
                    correctText.SetActive(true);
                }

                break;
            case 3:

                if (problemNumb1 * problemNumb2 == problemNumb3)
                {
                    correctText.SetActive(true);
                }

                break;
            case 4:

                if (problemNumb1 / problemNumb2 == problemNumb3)
                {
                    correctText.SetActive(true);
                }

                break;
        }

    }

    void ButtonClicked()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}
