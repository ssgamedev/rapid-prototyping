using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelDetect : MonoBehaviour
{

    public static bool numb1Present;
    public static bool numb2Present;
    public static bool numb3Present;

    public int panel;

    public static int panel1Result;
    public static int panel2Result;
    public static int panel3Result;

    void Start()
    {
        if (this.gameObject.name == "Panel 1")
        {
            panel = 1;
        }

        if (this.gameObject.name == "Panel 2")
        {
            panel = 2;
        }

        if (this.gameObject.name == "Panel 3")
        {
            panel = 3;
        }

    }

    private void OnCollisionEnter(Collision other)
    {
        switch (panel)
        {
            case 1:

                if (other.gameObject.name == "Numb1")
                {
                    panel1Result = 1;
                    numb1Present = true;
                }

                if (other.gameObject.name == "Numb2")
                {
                    panel1Result = 2;
                    numb2Present = true;
                }

                if (other.gameObject.name == "Numb3")
                {
                    panel1Result = 3;
                    numb3Present = true;
                }

                break;
            case 2:

                if (other.gameObject.name == "Numb1")
                {
                    panel2Result = 1;
                    numb1Present = true;
                }

                if (other.gameObject.name == "Numb2")
                {
                    panel2Result = 2;
                    numb2Present = true;
                }

                if (other.gameObject.name == "Numb3")
                {
                    panel2Result = 3;
                    numb3Present = true;
                }

                break;
            case 3:

                if (other.gameObject.name == "Numb1")
                {
                    panel3Result = 1;
                    numb1Present = true;
                }

                if (other.gameObject.name == "Numb2")
                {
                    panel3Result = 2;
                    numb2Present = true;
                }

                if (other.gameObject.name == "Numb3")
                {
                    panel3Result = 3;
                    numb3Present = true;
                }

                break;
        }
    }


}
